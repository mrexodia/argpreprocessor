#include <iostream>
#include <stdio.h>
#include "UString/UString.h"

static void printCharacters(const UString & command)
{
    for(unsigned int i=0; i<command.Length(); i++)
    {
        printf("command[%.2i]=", i);
        //print characters
        UString character=command[i];
        for(unsigned int j=0; j<character.DataLength(); j++)
            printf("%.2X ", (unsigned char)character.c_str()[j]);
        puts("");
    }
}

static void printUString(const UString & str)
{
    printf("\"%s\"\n", str.c_str());
}

struct LexArgumentChar
{
    UString ch;
    bool escaped;
};

struct LexArgument
{
    UString text;
    std::vector<LexArgumentChar> chars;
};

struct LexCommand
{
    UString text;
    LexArgument args;
};

static void preprocessor(const char* cmd)
{
    UString command(cmd);
    //remove prepended spaces
    command=command.Trim();
    printUString(command);
    //separate command and arguments
    UString arguments;
    for(unsigned int i = 0; i < command.Length(); i++)
        if(command[i] == " ")
        {
			arguments = command.Substring(i+1).Trim();
			command = command.Substring(0, i).Trim();
            break;
        }
    printUString(command);
    printUString(arguments);
}

int main()
{
    const char* command="     cmd      arg1,   arg2  ";
    printf("command:\n\"%s\"\n", command);
    preprocessor(command);
    return 0;
}
